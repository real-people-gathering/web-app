import { readable } from "svelte/store";
import { http, apiUrl } from "./http";

export const keycloak = new Keycloak();

export const authenticatedUser = readable(false, async function start(set) {
  const authenticated = await keycloak.init({
    onLoad: "check-sso",
    promiseType: "native",
    checkLoginIframe: false
  });
  if (!authenticated) await keycloak.login();
  const user = await keycloak.loadUserProfile();

  try {
    await createUser({
      id: keycloak.subject,
      fullname: user.firstName + " " + user.lastName,
      mail: user.email,
      username: user.username
    });
  } catch (err) {
    return set({ id: keycloak.subject, ...user });
  }
  return set({ id: keycloak.subject, ...user });
});

function createUser(user) {
  return http.post(`${apiUrl}/users`, user);
}
