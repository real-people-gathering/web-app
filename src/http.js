import axios from "axios";
import { keycloak } from "./auth.js";

export const apiUrl = "http://api.thegame.fail";

const axiosInstance = axios.create({
  baseUrl: apiUrl,
  timeout: 0 // File upload is slow so ignore timeouts
});

axiosInstance.interceptors.request.use(
  async config => {
    if (keycloak.isTokenExpired(5)) {
      await keycloak.updateToken(5);
    }
    config.headers.Authorization = "Bearer " + keycloak.token;
    return config;
  },
  err => Promise.reject(err)
);

export const http = axiosInstance;
