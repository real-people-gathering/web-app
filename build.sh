#!/bin/bash

if [ "$1" = "" ]; then
  printf 'the name of the docker image is missing ($1)'
  exit 1
fi
if [ "$2" = "" ]; then
  printf 'path to Dockerfile is missing ($2)'
  exit 1
fi

printf 'Building the application'
npm install && npm audit fix
npm run build || return 1

printf 'Building docker image'
docker build -t $1 $2
